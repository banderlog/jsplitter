﻿
from lrparsing import Grammar, Prio, Ref, Token, Tokens, TokenRegistry
import rules
import matching
#import jcconv

#print(hex(ord('思')))

class ExprGrammar(Grammar):
    class T(TokenRegistry):
        # 、 hiragana, full width uppercase latin, full width lowercase latin, latin
        identifier = Token(re='[\u3001\u3040-\u3097\u30A0-\u30F6\u4E00-\u9FFF\uFF21-\uFF3A\uFF41-\uFF5A]+(?:\[[a-z0-9]+\])?')
        identifier["eval"] = lambda n, token_factory: matching.get_token(n[1], token_factory)

    expr = Ref("expr")
    parentheses = '（' + expr + '）'
    brackets = '＜'+ expr + '＞'
    slash_op = expr << Token('／') << expr
    tilde_op = Token('〜') << expr
    star_op = expr << Token('＊')
    add_op = expr << Token('＋') << expr
    expr = T.identifier | brackets | parentheses | star_op | Prio(tilde_op, add_op, slash_op)
    START = expr

    #
    # How it's evalualted.
    #
    @classmethod
    def eval_node(cls, n, token_factory):
        return n[1] if "eval" not in n[0] else n[0]["eval"](n, token_factory)

    tilde_op["eval"]    = lambda n, token_factory: matching.AnyBefore(n[2])
    parentheses["eval"] = lambda n, token_factory: matching.Optional(n[2])
    slash_op["eval"]    = lambda n, token_factory: matching.select(n[1], n[3])
    add_op["eval"]      = lambda n, token_factory: matching.concatenate(n[1], n[3])
    brackets["eval"]    = lambda n, token_factory: n[2]
    star_op["eval"]     = lambda n, token_factory: matching.Repeat(n[1])

def parse(expr, token_factory, start_anywhere=False):
   rule = ExprGrammar.parse(expr, tree_factory=lambda n: ExprGrammar.eval_node(n, token_factory))
   rule = matching.AnyBefore(rule)
   return rule

#
# Entry point.
#
def main():
    for expr in ['ぬく＋〜つる＋（つる）', 'Ｎ＋つる[prt]']:
        res = ExprGrammar.parse(expr)
        #print ("%s = %s" % (expr, ExprGrammar.parse(expr, ExprGrammar.eval_node)))
        print(ExprGrammar.repr_parse_tree(res))

if __name__ == "__main__":
    main()
