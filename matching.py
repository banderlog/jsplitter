﻿
from collections import namedtuple

#MatchResult = namedtuple('MatchResult', ['start_position', 'length'])

NO_DEBUG = True

def debug_output(logger, token_string, current_position, rule, result):
    if logger is None:
        return
    source = (str(token) for token in token_string[current_position:])
    print(str(result), str(rule), ''.join(source) )

class Base:
    def __init__(self):
        pass

    def match(self, token_string, current_position, collector, logger):
        return None

    def __repr__(self):
        return self.__str__()


class Token(Base):
    def __init__(self, token_pattern):
        super().__init__()
        self.token_pattern = token_pattern

    def __str__(self):
        return str(self.token_pattern)

    def match(self, token_string, current_position, collector, logger):
        if current_position >= len(token_string):
            return None
        head = token_string[current_position]
        res = self.token_pattern.match(head)
        #logger('token: matching my {} against {}, result:{}'.format(self.token_pattern, head, res))
        debug_output(logger, token_string, current_position, self.token_pattern, res)
        if res:
            collector(head)
            return current_position + 1
        else:
            return None

class Optional(Base):
    def __init__(self, rule):
        super().__init__()
        self.rule = rule

    def __str__(self):
        return '（' + str(self.rule) + '）'

    def match(self, token_string, current_position, collector, logger):
        next_position = self.rule.match(token_string, current_position, collector, logger)
        #logger('optional: matching against {}, next pos:{}'.format(token_string, next_position))
        debug_output(logger, token_string, current_position, str(self), next_position is not None)
        return current_position if next_position is None else next_position

class Path:
    def __init__(self):
        self.captured_tokens = []

    def __str__(self):
        return str(self.captured_tokens)

    def __repr__(self):
        return str(self.captured_tokens)

    def capture(self, token):
        """
        Captures an intermediate token path in OR operator
        """
        self.captured_tokens.append(token)

    def finalize(self, collector):
        for token in self.captured_tokens:
            collector(token)

class Or(Base):
    IntResult = namedtuple('IntResult', ['next_position', 'captured_path'])
    def __init__(self, rules):
        assert(len(rules) > 1)
        super().__init__()
        self.rules = rules

    def __str__(self):
        return '／'.join(str(rule) for rule in self.rules)

    def match(self, token_string, current_position, collector, logger):
        """
        The longest branch wins
        """
        best_match = None #
        for index, rule in enumerate(self.rules):
            path = Path()
            next_position = rule.match(token_string, current_position, path.capture, logger)
            if next_position is not None:
               if best_match is not None:
                    if best_match.next_position < next_position:
                        best_match = Or.IntResult(next_position=next_position, captured_path=path)
               else:
                    best_match = Or.IntResult(next_position=next_position, captured_path=path)
        if best_match is not None:
            best_match.captured_path.finalize(collector)
            return best_match.next_position

class And(Base):
    #IntermediateResult = namedtuple('IntermediateResult', ['next_position', 'tokens'])
    def __init__(self, rules):
        assert(len(rules) > 1)
        super().__init__()
        self.rules = rules

    def __str__(self):
        return '＋'.join(str(rule) for rule in self.rules)

    def match(self, token_string, current_position, collector, logger):
        path = Path()
        for index, rule in enumerate(self.rules):
            next_position = rule.match(token_string, current_position, path.capture, logger)
            if next_position is None:
                return None
            current_position = next_position
        path.finalize(collector)
        return next_position

class AnyBefore(Base):
    def __init__(self, rule):
        super().__init__()
        self.rule = rule

    def __str__(self):
        return '〜' + str(self.rule)

    def match(self, token_string, current_position, collector, logger):
        """
        The 1st succeeded path wins
        """
        for pos in range(current_position, len(token_string)):
            res = self.rule.match(token_string, pos, collector, logger)
            if res is not None:
                return res
        return None

class Repeat(Base):
    def __init__(self, rule):
        super().__init__()
        self.rule = rule

    def __str__(self):
        return str(self.rule) + '＊'

    def match(self, token_string, current_position, collector, logger):
        """
        Rule repeated 1 or more times
        """
        matched = False
        next_position = current_position
        prev_position = current_position
        while next_position < len(token_string):
            next_position = self.rule.match(token_string, prev_position, collector, logger)
            assert(next_position is None or prev_position <= next_position)
            if next_position is None:
                break
            else:
                matched = True
            prev_position = next_position
        return prev_position if matched else None

def concatenate(a, b):
   if isinstance(a, And):
      return And(a.rules + [b])
   else:
      return And([a, b])

def select(a, b):
   if isinstance(a, Or):
      return Or(a.rules + [b])
   else:
      return Or([a, b])

def get_token(token_name, token_factory):
   return Token(token_factory(token_name))

def main():
    pass

if __name__ == '__main__':
    main()
