﻿
import rules
from pprint import pprint
import CaboCha

import pos
import predicate
from predicate import Equal
from tokens import Token, CommonNoun, Pronoun, \
                   Particle, Auxiliary, Verb, Adverb, \
                   DependentNoun, AdjectiveI, AdjectiveNa

parser = CaboCha.Parser()


tokens = {
'Ｎ'   : predicate.IsNoun(),
'Ｖ'   : predicate.IsVerb(),
'Ｖる' : predicate.IsVerb(),
'Ｖｍａｓｕ' : predicate.IsVerb(),
'Ｗｈ'  : predicate.IsQuestionWord(),
'、'   : predicate.IsPunctuation(),
'イＡく' : predicate.IsAdjConjunctiveForm(),
'ナＡ' : predicate.IsAdjectiveNa(),
'イＡい': predicate.IsAdjectiveI(),

'Ｎｕｍ' : predicate.IsNumber(),

'あと' : Equal(CommonNoun('あと')),
'あまり' : Equal(Adverb('あまり')),
'どうか'  : Equal(Adverb('どうか')),
'どう' : Equal(Adverb('どう')),
'いかが' : Equal(Adverb('いかが')),
'なぜ' : Equal(Adverb('なぜ')),
'まだ'  : Equal(Adverb('まだ')),
'もう'   : Equal(Adverb('もう')),

'お'   : Equal(rules.o_prefix),
'ご'   : Equal(rules.go_prefix),

'いくら' : Equal(CommonNoun('いくら')),
'い'    : Equal(Verb('い')),           # <---- odd! for いつ parsing

'いらっしゃる' : Equal(Verb('いらっしゃい' )),
'する'   : predicate.IsSuru(),

'こと'   : Equal(DependentNoun('こと')),
'ところ' : Equal(DependentNoun('ところ')),
'もの'   : Equal(DependentNoun('もの')),
'とき'   : Equal(DependentNoun('とき')),
'ほう'  : Equal(DependentNoun('ほう')),
'の'   : predicate.IsDictFormEqual('の'),

'いい' : Equal(AdjectiveI('いい')),
'ほしい' : Equal(AdjectiveI('ほしい')),

'ん' : predicate.IsDictFormEqual('ん'), # '名詞', '非自立'

'あいだ' : predicate.IsDictFormEqual('あいだ'),

'て' : predicate.IsDictFormEqual(set(['て', 'で'])),
#'で'  : predicate.IsDictFormEqual('で'),

'いけ' : predicate.IsDictFormEqual('いける'),

'で[aux]' : Equal(Auxiliary('で')),
'な[aux]' : Equal(Auxiliary('な')),
'一緒'  : predicate.IsDictFormEqual(set(['いける', 'いっしょう'])),

'なく' : Equal(AdjectiveI('なく')),

'ほか' : predicate.IsDictFormEqual('ほか'),
'前'   : predicate.IsDictFormEqual('前'),
'ありがとう' : predicate.IsDictFormEqual('ありがとう'),
'お願い'  : predicate.IsDictFormEqual('お願い'),
'どうぞ'  : predicate.IsDictFormEqual('どうぞ'),
'じゅう' : predicate.IsDictFormEqual('どうぞ'),
'中'    : predicate.IsDictFormEqual('中'),
'場合'  : predicate.IsDictFormEqual('場合'),
}

pronouns = [
'あれ',    # place & subject
'あそこ',
'あちこち',
'あちら',
'あっち',
'ここ',
'こちら',
'こっち',
'これ',
'これら',
'そこ',
'そっち',
'それ',
'それら',

'どこ',
'どこぞ',
'どちら',
'どっか',
'どっち',
'どれ',
'なに',
'なん',
'何',

'あたし',   # personal
'あなた',
'あんた',
'おのれ',
'おまえ',
'おれ',
'お前',
'きみ',
'てめぇ',
'てめえ',
'だれ',
'どなた',
'ぼく',
'みな',
'みなさん',
'みんな',
'やつ',
'わたくし',
'わたし',
'われ',
'われわれ',
'オレ',
'ボク',
'己',
'俺',
'僕',
'僕たち',
'僕ら',
'君',
'君たち',
'彼',
'彼ら',
'彼女',
'我々',
'我ら',
'皆',
'私',
'誰',
'貴方',
'あいつ',
'奴',
'奴ら',
'奴等',

'いくつ',   # other
'いずれ',
'いつ',
'かしこ',
'そっ',
'その他',
'よそ'
]

particles = [
'か', #副助詞／並立助詞／終助詞
'かぁ',  #終助詞
'かい', #終助詞
'かしら', #終助詞
'かも', #副助詞
#'から[conj]', #接続助詞
#'から[case]', #格助詞
'から',

'からには', #接続助詞
#'が[conj]', #接続助詞
#'が[case]', #格助詞
'が',
'くらい', #副助詞
'ぐらい', #副助詞
'け', #終助詞
'けど', #接続助詞
'けれど', #接続助詞
'けれども', #接続助詞
'こそ', #係助詞
'さ', #終助詞
'さえ', #係助詞
'さかい', #接続助詞
'し', #接続助詞
'しか', #係助詞
'じゃ', #副助詞
#'じゃ', #接続助詞
'じゃあ', #副助詞
'すら', #係助詞
'ずつ', #副助詞
'ぜ', #終助詞
'ぞ',   #係助詞 終助詞
'たって',   #接続助詞
'たり',   #並立助詞
'だけ',  #副助詞
'だって',   #副助詞
'だに',   #副助詞
'だの',   #並立助詞
'だり',   #並立助詞      見+たり
'ちゃ',   #接続助詞
'っけ',   #終助詞

'って',    #格助詞,連語
'っていう', #格助詞,連語
'ってな',  #格助詞,連語
'っと',    #格助詞,引用'
'ていう',  #格助詞,連語

'つつ', #接続助詞
#'て',  #接続助詞          uncomment if not above
#'て',  #格助詞,連語

'で',  #接続助詞
#'で',  #格助詞
#'で',  #終助詞
'でも',  #副助詞
'と',  #並立助詞
#'と',  #副詞化
#'と',  #接続助詞
#'と',  #格助詞
#'と',  #格助詞,引用'
'という',  #格助詞,連語
'といった',  #格助詞,連語
'とか',  #並立助詞
'とかいう',  #格助詞,連語
'として',  #格助詞,連語
'とも',  #副助詞
#'とも',  #接続助詞

'ど', #接続助詞
'どころか', #接続助詞
'ども', #接続助詞
'な', #終助詞
'なぁ', #終助詞
'なあ', #終助詞
'ながら', #接続助詞
'なぞ',  #副助詞
'など',  #副助詞
'なり',  #並立助詞
#'なり',  #副助詞
#'なり',  #接続助詞
'なんか',  #副助詞
'なんぞ', #副助詞
'なんて', #副助詞
#'に[adv]  # 副詞化
#'に[case] # 格助詞
#'に[spec] # 特殊
'に',

'ね',  #終助詞
'ねぇ',  #終助詞
'ねえ',  #終助詞
'ねん',  #終助詞
#'の[case]格助詞
#'の[final]終助詞
#'の[link]連体化
#'の',   #連体化
'のう',  #終助詞
'ので',  #接続助詞
'のに',  #接続助詞
'のみ',  #副助詞
'は',  #係助詞
'ば',  #接続助詞
'ばかり',  #副助詞
'ばっかり',  #副助詞
'へ',  #格助詞
'べ',  #終助詞
'ほど',  #副助詞
'まで',  #副助詞
'も',  #係助詞
'ものの',  #接続助詞
'もん',  #終助詞

'や', #接続助詞 並立助詞 係助詞
#'や[final]', #終助詞

#'やら[link] #並立助詞 副助詞 終助詞
'やら',
'よ',  #終助詞
'より', #格助詞
'よー', #終助詞
'わ', #終助詞
'を', #格助詞

#'ん',  #格助詞
#'ん',  #終助詞
'んで',  #接続助詞

'デ',  #格助詞
'ネ',  #終助詞
'ヨ',  #終助詞
'程',  #副助詞

'にあたって',  #格助詞,連語
'にあたる',  #格助詞,連語
'において',  #格助詞,連語
'における',  #格助詞,連語
'にかけて',  #格助詞,連語
'について',  #格助詞,連語
'につき',  #格助詞,連語
'につけ',  #格助詞,連語
'につれ',  #格助詞,連語
'につれて',  #格助詞,連語
'にて',  #格助詞
'にとって',  #格助詞,連語
'にまつわる',  #格助詞,連語
'にゃ',  #特殊
'によって',  #格助詞,連語
'により',  #格助詞,連語
'による',  #格助詞,連語
'にわたって',  #格助詞,連語
'にわたる',  #格助詞,連語
'に対し',  #格助詞,連語
'に対して',  #格助詞,連語
'に対する',  #格助詞,連語
'に従い',  #格助詞,連語
'に従って',  #格助詞,連語
'に関して',  #格助詞,連語
'に関する',  #格助詞,連語
'に際して',  #格助詞,連語
'をめぐって',  #格助詞,連語
'をもって',  #格助詞,連語
'を通して',  #格助詞,連語
'を通じて',  #格助詞,連語
'とともに',  #格助詞,連語
'と共に',  #格助詞,連語
]

auxiliaries = [
'たろ',
'だろ',
'ず',
'つ',

'でしょ',
'です',
'でし',

'ざる',

'ませ',
'まし',
'ましょ',

'なかろ',
'なきゃ',
'なけりゃ',
'なけれ',

'まい',
'ない',
'たくっ',
'ぬ',
'だ', 		#読んだ
'た', 		#忘れ　た
'たい',     #忘れ＋たい
'たかっ',
'たく',     #忘れ＋たく＋ない
'なかっ',   #忘れ＋なかっ＋た
'ごとき',
'ごとく',
'ござい',
'らしい',   #子供らしい
'らしく'
'らしかっ',
'べから',    #べからず
'べき',
'べく',
#'ん',     #　話し　ませ　ん   uncomment if 'ん' noun is removed
'ます',   #話し　ます
'う'       #　見よ＋う、話＋う
]

verbs = [
'させ',  #見させられる
'れる',  #話される　
'られる',  #見られる
'せる',    #話させられる　
'す',      #話さす
'さす',   # 見さす

'なる',  #
'ある',
'いる',
'できる',
'思う',
'みる',
'やる',
'くれる',
'あげる',
'もらう',
'なさる',

]

verbs_kanji = {
'くる' : ['来る'],
'いく' : ['行く']

}

for pronoun in pronouns:
    if pronoun in tokens:
        raise KeyError(pronoun)
    tokens[pronoun] = Equal(Pronoun(pronoun))

for particle in particles:
    if particle in tokens:
        raise KeyError(particle)
    tokens[particle] = Equal(Particle(particle))

for auxiliary in auxiliaries:
    if auxiliary in tokens:
        raise KeyError(auxiliary)
    tokens[auxiliary] = Equal(Auxiliary(auxiliary))

for verb in verbs:
    if verb in tokens:
        raise KeyError(verb)
    tokens[verb] = predicate.IsDictFormEqual(verb)

for verb, alternative_forms in verbs_kanji.items():
    if verb in tokens:
        raise KeyError(verb)
    allforms = set(alternative_forms + [verb])
    tokens[verb] = predicate.IsDictFormEqual(allforms)

def get_pattern(name):
    return tokens[name]

def get_tokens_as_words(text):
    tree = parser.parse(text)
    current_pos = 0
    for i in range(tree.token_size()):
        token = tree.token(i)
        features = list( token.feature_list(j) for j in range(token.feature_list_size) )  # token.normalized_surface
        assert(len(features) == 7 or len(features) == 9)
        w = Token(token.surface, pos.get_pos(features[0]), function=features[1],
             feature2=pos.normalize_feature(features[2]),
             feature3=pos.normalize_feature(features[3]),
             feature4=pos.normalize_feature(features[4]),
             feature5=pos.normalize_feature(features[5]),
             dictionary_form=features[6],
             position=current_pos
             )
        current_pos += len(token.surface)
        yield w

def get_tokens(text):
    tree = parser.parse(text)
    for i in range(tree.token_size()):
        token = tree.token(i)
        features = list( token.feature_list(j) for j in range(token.feature_list_size) )
        yield (token.surface, token.normalized_surface, features)


def dump_tokens_in_file():
    import os.path
    #text = 'B：いいえ、音楽と妊娠し'
    directory = r'c:\project\temp_for_mecab\1'
    file_name = 'C★NOVELS-創刊25周年アンソロジー.txt'
    file_name = os.path.join(directory, file_name)
    text = open(file_name, encoding='shift_jis', errors='ignore').read()
    res = set()
    for word in get_tokens_as_words(text):
        res.add(word.dump())
    for item in sorted(res):
        a = str(item)
        if a.find('[aux]') != -1:
            pprint(a)

    #for token in get_tokens(text):
    #    pprint(token)

def dump_tokens_in_text():
    text = '客：アイスクリームを一つお願いし'
    for token in get_tokens(text):
        pprint(token)

def main():
    dump_tokens_in_text()

if __name__ == '__main__':
    main()
