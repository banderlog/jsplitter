﻿# -*- coding: utf-8 -*-

import xml.etree.ElementTree as etree
from collections import namedtuple
import os.path
import graphviz
#from html import escape  # py3

Subgraph = namedtuple('Subgraph', 'subgraph_link_id head_node_id nodes edges')

class Token:
    def __init__(self, name, feature):
        #self.id = id
        self.name = name
        self.feature = feature

class Chunk:
    def __init__(self, id, link, func, head, tokens):
        self.id = id
        self.link = link
        self.head = head
        self.func = func
        self.tokens = tokens

    def get_head(self):
        return self.tokens[self.head]

    def get_func(self):
        return self.tokens[self.func]

    def get_pivot(self):
        #return chunk.head
        return max(self.tokens.keys())

    def get_link(self):
        return max(self.tokens.keys())

def get_chunks(root, skip_symbols=True):
    chunks = []
    for chunk_elem in root.findall('chunk'):
        chunk_id = int(chunk_elem.attrib['id'])
        link = int(chunk_elem.attrib['link'])
        func = int(chunk_elem.attrib['func'])
        head = int(chunk_elem.attrib['head'])
        tokens = {}
        for token_el in chunk_elem.findall('tok'):
            name = token_el.text
            token_id = int(token_el.attrib['id'])
#            name = chunk_elem.attrib['name']
            feature = token_el.attrib['feature']
            feature_id = get_feature_id(feature)
            if not skip_symbols or feature_id != FEATURE_SYMBOL:
                tokens[token_id] = Token(name, feature)
        chunks.append(Chunk(chunk_id, link, func, head, tokens))
    return chunks

def to_html_name(name):
    #return name
    n = name.encode('ascii', 'xmlcharrefreplace')
    return n.decode('ascii')

def get_node_type(chunk, token_id):
    if token_id == chunk.head:
        return Node.HEAD
    elif token_id == chunk.func:
        return Node.FUNC
    else:
        return Node.ORDINAL

def create_subgraph(chunk):
    edges = {}
    nodes = {}
    prev_node_id = None
    for token_id in sorted(chunk.tokens.keys()):
        token = chunk.tokens[token_id]
        node_id = token_id
        node_type = get_node_type(chunk, token_id)
        nodes[node_id] = Node(token.name, node_type)
        if prev_node_id is not None:
            edges[(node_id, prev_node_id)] = Edge('')
        prev_node_id = node_id
    head_node_id = chunk.get_pivot()
    return chunk.id, Subgraph(chunk.link, head_node_id, nodes, edges)

def create_graph(chunks):
    subgraphs = {}
    for chunk in chunks:
        subgraph_id, subgraph = create_subgraph(chunk)
        subgraphs[subgraph_id] = subgraph
    return merge_subgraphs(subgraphs)

FEATURE_SYMBOL = '記号'

def get_feature_id(feature):
    return feature.split(',')[0]

def get_link_node_from(subgraph):
    # return subgraph.head_node_id
    return min(subgraph.nodes.keys())

def get_link_node_to(subgraph):
    # return subgraph.head_node_id
    return max(subgraph.nodes.keys())

def merge_subgraphs(subgraphs):
    NO_PARENT_NODE = -1
    total_edges = {}
    total_nodes = {}
    for subgraph_id, subgraph in subgraphs.items():
        total_nodes.update(subgraph.nodes)
        total_edges.update(subgraph.edges)
        if subgraph.subgraph_link_id != NO_PARENT_NODE:
            parent_subgraph = subgraphs[subgraph.subgraph_link_id]
            from_node = get_link_node_from(parent_subgraph)
            to_node = get_link_node_to(subgraph)
            total_edges[(from_node, to_node)] = Edge('')
    return total_nodes, total_edges

class Node:
    ORDINAL = 0
    HEAD = 1
    FUNC = 2
    def __init__(self, name, node_type):
        self.name = name
        self.node_type = node_type

    def str(self):
        return 'Node({}, {})'.format(node_type, name)


class Edge:
    def __init__(self, name):
        #self.id = id
        self.name = name
        #self.from_edge_id = from_edge_id
        #self.to_from_id = to_edge_id

    def str(self):
        return 'Edge({})'.format(name)

shapes = {
      Node.ORDINAL : 'ellipse',
      Node.HEAD : 'diamond',
      Node.FUNC : 'box'
}

def to_dot(nodes, edges):

    out = 'digraph Sentence ' + \
          '{' + '\n' + \
          ' { rank = same;' + '\n'
    for node_id in sorted(nodes.keys()):
        node = nodes[node_id]
        name = to_html_name(node.name)
        out += ' {{ node [shape={0}, label="{1}"] N{2}; }} /* {3} */\n'.format(shapes[node.node_type], name, node_id, node.name)
    out += ' }\n'
    for edge_id in sorted(edges.keys()):
        edge = edges[edge_id]
        out += ' N{1} -> N{0};\n'.format(*edge_id)
    out += '}'
    return out

def load_chunks(file_name):
    tree = etree.parse(file_name)
    root = tree.getroot()
    chunks = get_chunks(root)
    return chunks

def load_graph(file_name):
    chunks = load_chunks(file_name)
    nodes, edges = create_graph(chunks)
    dot = to_dot(nodes, edges)
    return dot

def main():
    file_name = r'c:\project\cpp\cabtest\out.txt'
    dot = load_graph(file_name)
    print(dot)


if __name__ == '__main__':
    main()

