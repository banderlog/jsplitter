
# -*- coding: utf-8 -*-
from pprint import pprint
import CaboCha

import pos

parser = CaboCha.Parser()

class Token:
    def __init__(self, id, text, normilized_text, features):
        self.id = id
        self.text = text
        self.normilized_text = normilized_text
        self.features = features

    def __repr__(self):
        if self.text != self.normilized_text:
            return 'Token({}, {})'.format(self.text, self.normilized_text)
        return 'Token({})'.format(self.text)

class Chunk:
    def __init__(self, chunk_id, tokens, head, func, features):
        self.chunk_id = chunk_id
        self.tokens = tokens
        self.head_token_id = head
        self.func_token_id = func
        self.features = features

    def get_text(self):
        return ''.join(token.text for token in self.tokens)

    def __repr__(self):
        return 'Chunk(Tokens={}, Head={}, Func={}, Features={})'.format(
                    self.tokens, self.head_token_id, self.func_token_id, '')#' '.join(self.features))

    def get_features(self):
        return self.features
        return dict( feature.split(':') for feature in self.features )

    def get_head_token(self):
        return self.tokens[self.head_token_id]
        for token in self.tokens:
            if token.id == self.head_token_id:
                return token
        else:
            return None

    def get_func_token(self):
        return self.tokens[self.func_token_id]
        for token in self.tokens:
            if token.id == self.func_token_id:
                return token
        else:
            return None

class Sentence:
    def __init__(self, chunks, links, root_id):
        self.chunks = chunks
        self.links = links
        self.root_id = root_id

    def __repr__(self):
        return 'Sentence({}, {})'.format(self.chunks, self.links)

def split(sentence):
    tree = parser.parse(sentence)
    chunk_ids = {}
    links = []
    root_id = None
    for i in range(tree.chunk_size()):
        chunk = tree.chunk(i)
        features = list( chunk.feature_list(j) for j in range(chunk.feature_list_size) )
        to_link_id = chunk.link
        if to_link_id != -1:
            links.append((i, to_link_id))
        else:
            assert(root_id == None)
            root_id = i
        chunk_rec = (i, chunk.head_pos, chunk.func_pos, features, [])
        #print(chunk_rec)
        chunk_ids[chunk.token_pos] = chunk_rec

    last_chunk = None
    for i in range(tree.token_size()):
        token = tree.token(i)
        if token.chunk is None:
            parent_chunk = last_chunk
        else:
            parent_chunk = chunk_ids.get(token.chunk.token_pos, None)
            last_chunk = parent_chunk
        features = list( token.feature_list(j) for j in range(token.feature_list_size) )
        parent_chunk[-1].append(Token(i, token.surface, token.normalized_surface, features))

    chunks = {}
    for token_pos_id in sorted(chunk_ids.keys()):
        chunk_id, head_pos, func_pos, features, my_tokens = chunk_ids[token_pos_id]
        chunks[chunk_id] = Chunk(chunk_id, my_tokens, head_pos, func_pos, features)

    return Sentence(chunks, links, root_id)

def dump(text):
    tree = parser.parse(text)
    return tree.toString(CaboCha.CABOCHA_FORMAT_XML)

def get_tokens(text):
    tree = parser.parse(text)
    for i in range(tree.token_size()):
        token = tree.token(i)
        features = list( token.feature_list(j) for j in range(token.feature_list_size) )
        yield (token.surface, token.normalized_surface, features)
