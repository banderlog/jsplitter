﻿import pos
import token

# Matching predicate classes

class Predicate:
    def match(self, rhs):
        return False

class Equal(Predicate):
    def __init__(self, lhs):
        self.lhs = lhs

    def match(self, rhs):
        if self.lhs.function is None or rhs.function is None:
            return (self.lhs.word, self.lhs.pos) == \
                   (rhs.word, rhs.pos)
        else:
            return (self.lhs.word, self.lhs.pos, self.lhs.function) == \
               (rhs.word, rhs.pos, rhs.function)

    def __repr__(self):
        return 'Equal(' + repr(self.lhs)+ ')'

class IsNoun(Predicate):
    def match(self, rhs):
        return rhs.pos == pos.NOUN

    def __repr__(self):
        return 'IsNoun()'

class IsNumber(Predicate):
    def match(self, rhs):
        return rhs.function == pos.FunctionFeature.NUMBER

    def __repr__(self):
        return 'IsNumber()'

class IsVerb(Predicate):
    def match(self, rhs):
        return (rhs.pos == pos.VERB and rhs.word != 'て') or \
               (rhs.pos == pos.AUXILIARY and rhs.word != 'た')

    def __repr__(self):
        return 'IsVerb()'

class IsAdverb(Predicate):
    """
    Is the word an adverb
    """
    def match(self, rhs):
        return rhs.pos == pos.ADVERB

    def __repr__(self):
        return 'IsAdverb()'

class IsAdjConjunctiveForm(Predicate):
    """
    e.g. 欲しく
    """
    def match(self, rhs):
        return rhs.feature5 == '連用テ接続'

    def __repr__(self):
        return 'IsAdjConjunctiveForm()'

class IsAdjectiveI(Predicate):
    """
    e.g. 欲しく
    """
    def match(self, rhs):
        return rhs.pos == pos.ADJI

    def __repr__(self):
        return 'IsAdjectiveI()'

class IsAdjectiveNa(Predicate):
    """
    e.g. 欲しく
    """
    def match(self, rhs):
        return rhs.pos == pos.ADJNA # or rhs.pos == pos.NOUN  # Does IsNoun suit better?

    def __repr__(self):
        return 'IsAdjectiveNa()'

class IsPunctuation(Predicate):
    def match(self, rhs):
        return rhs.pos == pos.SYMBOL

    def __repr__(self):
        return 'IsPunctuation()'

class IsQuestionWord(Predicate):
    def match(self, rhs):
        return rhs.word in ('誰', '何', 'どこ', 'なに','なん', 'だれ')

    def __repr__(self):
        return 'IsQuestionWord()'

class IsSuru(Predicate):
    def match(self, rhs):
        return rhs.feature4 == 'サ変・スル'

    def __repr__(self):
        return 'IsSuru()'

class IsDictFormEqual(Predicate):
    def __init__(self, dictionary_form):
        self.dictionary_form = dictionary_form

    def match(self, rhs):
        if isinstance(self.dictionary_form, set):
            return rhs.dictionary_form in self.dictionary_form
        else:
            return self.dictionary_form == rhs.dictionary_form

    def __repr__(self):
        return 'IsDictFormEqual(' + str(self.dictionary_form) + ')'