﻿import pos
from tokens import Token, CaseParticle


# Particles
# case particles
de_particle = CaseParticle('で')
o_particle  = CaseParticle('を')
ga_particle = CaseParticle('が')
e_particle = CaseParticle('へ')

# conjunctive, final
ka_particle = Token('か', pos.PARTICLE, function=pos.FunctionFeature.MIXED_PARTICLE)

# final particles
yo_final_particle = Token('よ', pos.PARTICLE, function=pos.FunctionFeature.FINAL_PARTICLE)

# conjunctive
no_particle = Token('の', pos.PARTICLE, function=pos.FunctionFeature.UNION_MOD)
de_conj_particle = Token('で', pos.PARTICLE, function=pos.FunctionFeature.CONJUNCTIVE_PARTICLE)
te_conj_particle = Token('て', pos.PARTICLE, function=pos.FunctionFeature.CONJUNCTIVE_PARTICLE)
wa_conj_particle = Token('は', pos.PARTICLE, function=pos.FunctionFeature.CONJUNCTIVE_PARTICLE)

o_prefix = Token('お', pos.PREFIX)
go_prefix = Token('ご', pos.PREFIX)

kudasai = Token('ください', pos.VERB, function=pos.FunctionFeature.DEPENDENT, dictionary_form='ださる')
suru = Token('する', pos.VERB, function=pos.FunctionFeature.INDEPENDENT)
nante = Token('なんて', pos.PARTICLE, function=pos.FunctionFeature.ADVERB)
rare = Token('られ', pos.VERB, function=pos.FunctionFeature.SUFFIX)
mase = Token('ませ', pos.AUXILIARY)

kuru_verb = rare = Token('来る', pos.VERB, function=pos.FunctionFeature.SUFFIX)

def to_none(feature):
    return None if feature == '*' else feature

def get_word(word, features):
    pos_jname, function_name, \
    feature2, feature3, feature4, \
    feature5, normal_form, _, _      = features
    pos_enum = pos.get_pos(pos_jname)
    function_name = to_none(function_name)
    feature2 = to_none(feature2)
    feature3 = to_none(feature3)
    feature4 = to_none(feature4)
    feature5 = to_none(feature5)

    return Token(word, pos_enum, function=function_name,
                feature2=feature2, feature3=feature3, feature4=feature4,
                feature5=feature5,
                normal_form=normal_form)

def main():
    pass

if __name__ == '__main__':
    main()
