﻿
# -*- coding: utf-8 -*-

import os.path
import unittest
import sys
import pprint
import re
sys.path.append(os.path.abspath('..'))

from matching import AnyBefore
import ruleparser
import rulelist
import tokenfactory
import splitter

class Collector:
    def __init__(self):
        self.words = []

    def capture(self, token):
        self.words.append(token.word)

    def result(self):
        return self.words

class TestRuleParsing(unittest.TestCase):
    def setUp(self):
      pass

    def run_test_only(self, sentence, rule_expr, log=False):
      if log:
        print('')
        print('-' * 25)
      rule = ruleparser.parse(rule_expr, tokenfactory.get_pattern)
      rule = AnyBefore(rule)
      tokens = tokenfactory.get_tokens_as_words(sentence)
      tokens = list(tokens)
      #print(tokens)
      logger = lambda x: print(x)
      no_logger = None
      collector = Collector()
      res = rule.match(tokens, 0, collector.capture, logger if log else no_logger)
      return res, collector.result()

    def run_test(self, sentence, rule_expr, expected_result, expected_position, log=False):
      position, token_list = self.run_test_only(sentence, rule_expr, log)
      self.assertEqual(expected_position, position)
      self.assertEqual(expected_result, token_list)

    def token_factory(self, token_name):
        return token_name

    def testRuleListParsing(self):
        for expr in rulelist.rules:
            rule_objs = ruleparser.parse(expr, self.token_factory)
            expected_expr = re.sub('[＜＞]', '', expr)
            self.assertEqual(expected_expr, str(rule_objs))

    def testMatching(self):
      rule = 'Ｎ＋〜で'
      sentence = '食事のあとで、この薬を飲んでください。'
      self.run_test(sentence, rule, ['食事', 'で'], 4)

      rule = 'あと＋〜で'
      sentence = '食事のあとで、この薬を飲んでください。'
      self.run_test(sentence, rule, ['あと', 'で'], 4)

      rule = 'お＋Ｎ／ご＋Ｎ'
      sentence = '彼、何度も……ご両親、お好き、ありがとう。'
      self.run_test(sentence, rule, ['ご', '両親'], 9)

      sentence = '彼、何度も…お好き、ありがとう。'
      self.run_test(sentence, rule, ['お', '好き'], 8)

    def testNounAtoDe(self):
      rule = 'Ｎ＋の＋あと＋で'
      sentence = '食事のあとで、この薬を飲んでください。'
      self.run_test(sentence, rule, ['食事', 'の', 'あと', 'で'], 4)

    def testVerbAtoDe(self):
        sentence = '映画が終ったあとで、喫茶店へ行きました。'
        rule = 'Ｖ＊＋た＋あと＋で'
        self.run_test(sentence, rule, ['終っ', 'た', 'あと', 'で'], 6)

        sentence = '映画が取り出していたあとで、喫茶店へ行きました。'
        rule = 'Ｎ＋の＋あと＋で／Ｖ＋（＜て＋Ｖ＞＊）＋た＋あと＋で'#'Ｖ＊＋た＋あと＋で'
        self.run_test(sentence, rule, ['取り出し', 'て', 'い', 'た', 'あと', 'で'], 8)

    # Expressions from grammar list

    def testDochiraga(self):
      rule = rulelist.rules[0]
      sentence = 'マンガ 餃子屋と高級フレンチでは、どちらが儲かるか?'
      self.run_test(sentence, rule, ['と', 'どちら', 'が', 'か'], 13)

    def testAtoDe(self):
        rule = rulelist.rules[1]
        sentence = '食事のあとで、この薬を飲んでください。'
        self.run_test(sentence, rule, ['食事', 'の', 'あと', 'で'], 4)

    def testAmari(self):
        rule = rulelist.rules[2]
        text = 'A:あなたはよく音楽を聞きますか。B：いいえ、音楽はあまり聞きません。'
        self.run_test(text, rule, ['あまり', 'ん'], 21)

    def testIkutsu(self):
        rule = rulelist.rules[3]
        text = 'たまごはいくつ買いますか。'
        self.run_test(text, rule, ['いくつ', 'か'], 7)

    def testIkura(self):
        rule = rulelist.rules[4]
        text = '飛行機で大阪へ行きたいんですが、いくらぐらいかかりますか。'
        self.run_test(text, rule, ['いくら', 'か'], 15)

    def testItsu(self):
        rule = rulelist.rules[5]
        text = '試験はいつですか。来月です。'
        self.run_test(text, rule, ['い', 'つ', 'か'], 6)

    def testIrassharu(self):
        rule = rulelist.rules[6]
        text = '今、山田先生は教室にいらっしゃいます。'
        self.run_test(text, rule, ['教室', 'に', 'いらっしゃい'], 8)

    def testPolitePrefix(self):
        rule = rulelist.rules[7]
        text = '課長、お電話ですよ。'
        self.run_test(text, rule, ['お', '電話'], 4)

    def testKaNaiKa(self):
        rule = rulelist.rules[8]
        text = 'このりんごはおいしいかおいしくないか分かりません。'
        self.run_test(text, rule, ['か', 'ない', 'か'], 8)

    def testKaDouKa(self):
        rule = rulelist.rules[9]
        text = 'このりんごはおいしいかどうか分かりません。'
        self.run_test(text, rule, ['か', 'どうか'], 6)

    def testVerbKara(self):
        rule = rulelist.rules[10]
        text = '今、仕事の時間ですから、課長は事務室にいるでしょう。'
        self.run_test(text, rule, ['です', 'から'], 7)

    def testNounKara(self):
        rule = rulelist.rules[11]
        text = '春休みは何日から始まりますか。'
        self.run_test(text, rule, ['日', 'から'], 5)

    def testVerbTeKara(self):
        rule = rulelist.rules[12]
        text = 'ちょっと待ってください。手を洗ってから作ります。'
        self.run_test(text, rule, ['洗っ', 'て', 'から'], 10)

    def testWhWordKa(self):
        rule = rulelist.rules[13]
        text = '今、教室にだれかがいますか。'
        self.run_test(text, rule, ['だれ', 'か'], 6)

    def testKaFinalParticle(self):
        rule = rulelist.rules[14]
        text = 'A：このケータイ、君のですか。'
        self.run_test(text, rule, ['か', '。'], 10)

    def testGaCaseParticle(self):
        rule = rulelist.rules[15]
        text = '春が来ました。'
        self.run_test(text, rule, ['春', 'が'], 2)

    def testGaConjunction(self):
        rule = rulelist.rules[16]
        text = 'わたしは日本語はできますが、英語は少しもできない。'
        self.run_test(text, rule, ['が', '、'], 8)

    def testAdverbOfAdjIWithSuru(self):
        rule = rulelist.rules[17]
        text = 'ストーブをつけて部屋を暖かくしました。'
        self.run_test(text, rule, ['暖かく', 'し'], 8)

    def testAdverbOfAdjIWithNaru(self):
        rule = rulelist.rules[18]
        text = 'これからだんだん寒くなります。'
        self.run_test(text, rule, ['寒く', 'なり'], 4)

    def testAdverbOfAdjIWithNaru(self):
        rule = rulelist.rules[19]
        text = '30分ぐらい待ちましたが、バスは来ませんでした。'
        self.run_test(text, rule, ['ぐらい'], 3)

    def testKeredomo(self):
        rule = rulelist.rules[20]
        text = 'わたしは魚は食べますけれども、肉はあまり食べません。'
        self.run_test(text, rule, ['けれども'], 7)

    def testRuKotoGaAru(self):
        rule = rulelist.rules[21]
        text = '会社まで近いので、ときどき自転車で行くことがあります。'
        self.run_test(text, rule, ['行く', 'こと','が', 'あり'], 12)

    def testKotoGaDekiru(self):
        rule = rulelist.rules[22]
        text = 'あなたはピアノを弾くことができますか。'
        self.run_test(text, rule, ['弾く', 'こと','が', 'でき'], 8)

    def testTaKotoGaAru(self):
        rule = rulelist.rules[23]
        text = 'A：鈴木さん、あなたは中国へ旅行に行ったことがありますか。'
        self.run_test(text, rule, ['行っ', 'た', 'こと', 'が', 'あり'], 16)

    def testShikaNai(self):
        rule = rulelist.rules[24]
        text = '学生は五人いますが、ケーキは兰つしかありません。'
        self.run_test(text, rule, ['しか', 'ん'], 16)

    def testShikaNai(self):
        rule = rulelist.rules[24]
        text = '学生は五人いますが、ケーキは兰つしかありません。'
        self.run_test(text, rule, ['しか', 'ん'], 16)

    def testDzutsu(self):
        rule = rulelist.rules[25]
        text = '田中さんが私たちにりんごを2個ずつくれました。'
        self.run_test(text, rule, ['ずつ'], 11)

    def testVerbVolutional(self):
        rule = rulelist.rules[26]
        text = 'A：ここで少し休みましょう。'
        self.run_test(text, rule, ['ましょ', 'う'], 8)

    def testTaiToOmou(self):
        rule = rulelist.rules[27]
        text = 'わたしは日本人の会社で働きたいと思います。'
        self.run_test(text, rule, ['働き', 'たい', 'と', '思い'], 10)

    def testTaiNDesu(self):
        rule = rulelist.rules[28]
        text = 'ちょっと聞きたいんですが、駅まではどう行きますか。'
        self.run_test(text, rule, ['聞き', 'たい', 'ん', 'です', 'が'], 6)

    def testTari(self):
        rule = rulelist.rules[29]
        text = '太郎はテレビをつけたり、消したりしている。'
        self.run_test(text, rule, ['つけ', 'たり', '、', '消し', 'たり', 'し'], 10)

    def testIkenai(self):
        rule = 'Ｖ＋て＋は＋いけ＋＜ない／〜ん＞'
        text = 'ほかの人の手紙を読んではいけません'
        self.run_test(text, rule, ['読ん', 'で', 'は', 'いけ', 'ん'], 12)

    def testDake(self):
        rule = '（だけ）＋で[aux]＋いい'
        text = '一台だけでいいです。'#'A:車は何台で必要ですか。一台だけでいいです。'
        self.run_test(text, rule, ['だけ', 'で', 'いい'], 5)

    def testNakuteIi(self):
        rule = '＜Ｎ＋で[aux]／ナＡ＋で[aux]／イＡく＞＋（は）＋なく＋て'
        text = 'これは90点ではなくて、60点ですよ'
        self.run_test(text, rule, ['点', 'で', 'は', 'なく', 'て'], 8)

    def testDou(self):
        rule = 'どう／いかが'
        text = '日本語の勉強はどうですか'
        self.run_test(text, rule, ['どう'], 5)

    def testNoDesuKa(self):
        rule = '＜Ｎ／ナＡ＋な／イＡい／Ｖる＞＋の＋＜だ／は／が／を＞'
        text = 'あのパンは今日のだ'
        self.run_test(text, rule, ['今日', 'の', 'だ'], 6)

    def testNoDesuKa(self):
        rule = '＜Ｎ＋な／ナＡ＋な／イＡい／Ｖる／Ｖ＋た＞＋＜の＋だ／ん＋だ／の＋です／ん＋です＞＋か'
        text = 'その写真はここで取ったんですか'
        self.run_test(text, rule,  ['取っ', 'た', 'ん', 'です', 'か'], 10)



    def testMultipleRules(self):
        #return
        for rule, sample in rulelist.to_rules():
            try:
                position, token_list = self.run_test_only(sample, rule, False)
                if len(token_list) == 0:
                    print(rule, sample)
            except:
                print(rule, sample)
                raise

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRuleParsing)
    unittest.TextTestRunner(verbosity=2).run(suite)