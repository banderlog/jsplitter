﻿# -*- coding: utf-8 -*-

import os.path
import unittest
import sys
import pprint
import re

sys.path.append(os.path.abspath('..'))
import splitter
from textparser import TextParser
from syntax_tree import LayeredTree
import os

import graphviz

class SourceFile(graphviz.files.File):
    def __init__(self, dot_text):
        super().__init__(self)
        self.source = dot_text
        self._engine = r'c:\Program Files (x86)\Graphviz2.38\bin\dot'

    def get_svg(self):
        return self.pipe(format='svg').decode(self._encoding)

class TestSplitter(unittest.TestCase):
    def setUp(self):
      pass

    def testSplit(self):
      text = "太郎はこの本を二郎を見た女性に渡した。"
      sentence = splitter.split(text)
      #pprint.pprint(sentence.chunks)
      self.assertEqual(7, len(sentence.chunks))
      self.assertEqual(2, len(sentence.chunks[0].tokens))
      self.assertEqual('太郎', sentence.chunks[0].tokens[0].text)
      self.assertEqual('本を', sentence.chunks[2].get_text())
      self.assertEqual(6, len(sentence.links))
      self.assertEqual(6, sentence.root_id)
      #print(sentence.links)

    def testLayers(self):
      text = "太郎はこの本を二郎を見た女性に渡した。"
      sentence = splitter.split(text)
      tree = LayeredTree(sentence)
      layer_ids = tree.get_layers()
      self.assertEqual( [{6: {0, 5}}, {5: {4}}, {4: {2, 3}}, {2: {1}}], layer_ids)
      layer_texts = list(tree.get_layer_text('|'))
      self.assertEqual([ '太郎は|この本を二郎を見た女性に|渡した。',
                         'この本を二郎を見た|女性に',
                         'この本を|二郎を|見た',
                         'この|本を' ],
                         layer_texts[1:])

      #for layer in tree.get_layer_chunk_ids():
      #  pprint.pprint(layer)
      #for text in tree.get_layer_text('|'):
      #  pprint.pprint(text)

    def testLa(self):
      text = "たとえば政治的な人は多く、将軍などといったような人もかなりに多い"
      sentence = splitter.split(text)
      for chunk_id, chunk in sorted(sentence.chunks.items()):
        head_token = chunk.get_head_token()
        func_token = chunk.get_func_token()
        print(chunk.get_text(),
             head_token.text, head_token.features[0:2],
             func_token.text, func_token.features[0:2])
      print(splitter.dump(text))
      tree = LayeredTree(sentence)
   #   for text in tree.get_layer_text('|'):
   #     pprint.pprint(text)

    def testLayzz2(self):
      text = "たとえば政治的な人は多く、将軍などといったような人もかなりに多い"
      sentence = splitter.split(text)
      tree = LayeredTree(sentence)
   #   layers = tree.get_layers()
  #    for level, layer in sorted(layers.items()):
  #      pprint.pprint(layer)
      dot = tree.to_dot()
      #print('\n', dot)
   #   print(tree.get_nodes_per_layer())
      #print(tree.get_layers())


    complex_text = """
      「ああ、そうだ！」またもやある一つの考えをとらえたらしく、またそれをなくしては大変だというように、床の上に起き上がりさえもして、公爵は以前のように興奮して、急にせかせかした声でささやいた、「そうだ……僕は聞きたいと思ってたんだが……あのカルタは！　カルタは……君はあれとカルタをして遊んだじゃないか？」
      「うん」しばらくの沈黙ののち、ラゴージンはこう言った。
      """
    def testMulti(self):
      tp = TextParser(TestSplitter.complex_text)
      sentences = list(tp.getSentences())
      self.assertEqual(
      ['ああ、そうだ',
       '」またもやある一つの考えをとらえたらしく、またそれをなくしては大変だというように、床の上に起き上がりさえもして、公爵は以前のように興奮して、急にせかせかした声でささやいた、「そうだ',
       '僕は聞きたいと思ってたんだが',
        'あのカルタは',
        'カルタは',
        '君はあれとカルタをして遊んだじゃないか',
        'うん',
        'しばらくの沈黙ののち、ラゴージンはこう言った'
       ],
       sentences)

    def testMulti(self):
      text = '彼を罠にかけようという下心が、まるで無かったとは言えないにしても、あれは案外、生活の必要からきた、ごく日常的な習慣だったのかもしれないのだ。'
      tp = TextParser(text)
      overall = '<html><head><title>Sentence Graphs</title></head><body>'
      for index, sentence_text in enumerate(tp.getSentences()):
        sentence = splitter.split(sentence_text)
        tree = LayeredTree(sentence)
        dot = tree.to_dot()
        graph = SourceFile(dot)
        #file_name = graph.render()
        svg = graph.get_svg()
        svg = re.sub('(?:<\?.+?\?>|<!D.+?>)', '', svg, count=0, flags=re.DOTALL)

        overall += svg
        #with open(path) as file:
        #    overall += file.read()
        #os.unlink(path)
      overall += '</html></body>'
      with open('outz.html', mode='w', encoding='utf-8') as f:
        f.write(overall)

     # print('\n', dot)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSplitter)
    unittest.TextTestRunner(verbosity=2).run(suite)
