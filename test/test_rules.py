﻿# -*- coding: utf-8 -*-

import os.path
import unittest
import sys
import pprint
import re

sys.path.append(os.path.abspath('..'))
import splitter
import rules
import rulelist
import ruleparser

# PoS, function, function 2, , , , normal, kana, kana

class Collector:
    def __init__(self):
        self.words = []

    def capture(self, token):
        self.words.append(token.word)

    def result(self):
        return self.words

class TestRules(unittest.TestCase):
    def setUp(self):
      pass

    def testGetFeatures(self):
        return
        directory = r'c:\project\temp_for_mecab\1'
        file_name = 'C★NOVELS-創刊25周年アンソロジー.txt'
        file_name = os.path.join(directory, file_name)
        text = open(file_name, encoding='shift_jis', errors='ignore').read()
        dump = splitter.dump(text)
        pos = set()
        for line in dump.split('\n'):
            if len(line) and line[0] != '*' and len(line) > 5:
                word, feature = line.split('\t')
                parts = feature.split(',')
                pos.add(parts[5])
        print(pos)

    def testFeature(self):
        #return
        text = '彼、何度も……ご両親、お好き、ありがとう。'
        tokens = list(splitter.get_tokens(text))
        for token_word, _, features in tokens:
            #print(len())
            word = rules.get_word(token_word, features)
            #print(word, features)

    def testRuleGrammarParser(self):
        def create_token(tok):
            return '(' + tok + ')'
        expr = 'ぬく＋〜つる＋（つる）'
        res = ruleparser.parse(expr, create_token)
        self.assertEqual('(ぬく)＋〜(つる)＋（(つる)）', str(res))

        expr = 'Ｎ＋つる[prt]'
        res = ruleparser.parse(expr, create_token)
        self.assertEqual('(Ｎ)＋(つる[prt])', str(res))

        expr = '＜Ｎ／つ＞＋る[prt]'
        res = ruleparser.parse(expr, create_token)
        self.assertEqual('(Ｎ)／(つ)＋(る[prt])', str(res))

        expr = 'Ｎ＋つ＊＋る[prt]'
        res = ruleparser.parse(expr, create_token)
        self.assertEqual('(Ｎ)＋(つ)＊＋(る[prt])', str(res))


    def testRuleToTo(self):
        text = ''



if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRules)
    unittest.TextTestRunner(verbosity=2).run(suite)
