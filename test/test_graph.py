﻿# -*- coding: utf-8 -*-

import os.path
import unittest
import sys
import pprint

sys.path.append(os.path.abspath('..'))
from cabochatree import *

data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')

class TestGraph(unittest.TestCase):
    def setUp(self):
      pass

    def testChunkList(self):
      file_name = os.path.join(data_dir, 'test1.txt')
      chunks = load_chunks(file_name)
      self.assertEqual(3, len(chunks))
      chunk = chunks[1]
      self.assertEqual('2', chunk.link)
      self.assertEqual('5', chunk.head)
      self.assertEqual('6', chunk.func)
      self.assertEqual(4, len(chunk.tokens))
      token = chunks[1].tokens["6"]
      self.assertEqual('な', token.name)
      self.assertEqual('助動詞,*,*,*,特殊・ダ,体言接続,だ,ナ,ナ', token.feature)

    def testCreateSubgraph(self):
      file_name = os.path.join(data_dir, 'test1.txt')
      chunks = load_chunks(file_name)
      chunk = chunks[1]
      subgraph_id, subgraph = create_subgraph(chunk)
      self.assertEqual('1', subgraph_id)
      self.assertEqual('1_6', subgraph.head_node_id)
      self.assertEqual('2', subgraph.subgraph_link_id)
      self.assertEqual(4, len(subgraph.nodes))
      self.assertEqual(3, len(subgraph.edges))

    def testCreateGraph(self):
      file_name = os.path.join(data_dir, 'test1.txt')
      chunks = load_chunks(file_name)
      nodes, edges = create_graph(chunks)
      self.assertEqual(10, len(nodes))
      self.assertEqual(9, len(edges))
      self.assertIn(('2_8', '0_2'), edges)
      self.assertIn(('2_8', '1_6'), edges)
     # pprint.pprint(nodes)
     # pprint.pprint(edges)

    def testDotConversion(self):
      file_name = os.path.join(data_dir, 'test1.txt')
      dot = load_graph(file_name)
      print(dot)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGraph)
    unittest.TextTestRunner(verbosity=2).run(suite)
