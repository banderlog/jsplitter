﻿# -*- coding: utf-8 -*-

import os.path
import unittest
import sys
import pprint
import re

sys.path.append(os.path.abspath('..'))

import matching
from matching import Or, And, AnyBefore, Optional
#from matching import MatchResult

class Aggregator:
    def __init__(self):
        self.matched_expr = ''

    def add(self, token):
        self.matched_expr += token

    def result(self):
        return self.matched_expr

class TestTokenPattern:
   def __init__(self, sym):
      self.sym = sym

   def match(self, text):
      return self.sym == text

   def __str__(self):
      return self.sym

class TestMatching(unittest.TestCase):
    def setUp(self):
        pass

    def get_token(self, sym):
        return TestTokenPattern(sym)

    def log(self, text):
        pass

    def match(self, text, rule, end_position, matched_text):
        aggregator = Aggregator()
        res = rule.match(text, 0, aggregator.add, self.log)
        self.assertEqual(end_position, res)
        self.assertEqual(matched_text, aggregator.result())

    def testSingleWord(self):
        what = self.get_token('W')
        occ1 = matching.Token(what)
        text = what.sym + 'S'

        self.match('W', occ1, 1, 'W')
        self.match('KS', occ1, None, '')
        self.match('PKsuffix', occ1, None, '')

    def testOptional(self):
        what = 'W'
        t = self.get_token(what)
        opt1 = matching.Optional(matching.Token(t))

        self.match('WS', opt1, 1, 'W')
        self.match('KS', opt1, 0, '')

    def testOr(self):
        what1 = 'W'
        what2 = 'T'
        or1 = matching.Or([matching.Token(self.get_token(what1)),
                           matching.Token(self.get_token(what2))])

        text = what1 + 'S'
        self.match(text, or1, len(what1), what1)

        text = what2 + 'S'
        self.match(text, or1, len(what2), what2)

        text = what2 + what1 + 'S'
        self.match(text, or1, len(what2), what2)

        self.match('KS', or1, None, '')

        what1 = self.get_token('W')
        what2 = self.get_token('L')
        or1 = matching.Or([matching.Token(what1), matching.Token(what2)])

        self.match(what2.sym + what1.sym, or1, len(what2.sym), what2.sym)

    def testAnd(self):
        what1 = self.get_token('W')
        what2 = self.get_token('T')
        andRule = matching.And([matching.Token(what1), matching.Token(what2)])

        text = 'WT'
        self.match(text, andRule, len(text), text)

        text = 'W' + 'a' + 'T'
        self.match(text, andRule, None, '')

    def testAnyPrefix(self):
        what1 = self.get_token('W')
        what2 = self.get_token('T')
        starRule = matching.And([matching.AnyBefore(matching.Token(what1)), matching.Token(what2)])

        text = what1.sym + what2.sym
        self.match(text, starRule, len(text), text)

        text = 'a' + what1.sym + what2.sym
        self.match(text, starRule, 3, 'WT')

    def tesAnyInside(self):
        what1 = self.get_token('W')
        what2 = self.get_token('T')
        starRule = matching.And([matching.Token(what1), matching.AnyBefore(matching.Token(what2))])

        text = what1.sym + what2.sym
        self.match(text, starRule, len(text), text)

        text = what1.sym + 'ANZ' + what2.sym
        self.match(text, starRule, len(text), 'WT')

        text = what1.sym + 'KKK' + 'LL'
        self.match(text, starRule, None, '')

    def testAnyInFront(self):
        W = matching.Token(self.get_token('W'))
        T = matching.Token(self.get_token('T'))
        starRule = matching.And([matching.AnyBefore(W), matching.AnyBefore(T)])

        text = 'preWT'
        self.match(text, starRule, len(text), 'WT')

        text = 'WANZT'
        self.match(text, starRule, len(text), 'WT')

        text = 'WKKKLL'
        self.match(text, starRule, None, '')

    def testRepeat(self):
        W = matching.Token(self.get_token('W'))
        T = matching.Token(self.get_token('T'))
        rule = matching.Repeat(W)

        text = 'WT'
        self.match(text, rule, 1, 'W')

        text = 'WWT'
        self.match(text, rule, 2, 'WW')

        text = 'abcWWT'
        self.match(text, matching.AnyBefore(rule), 5, 'WW')

        rule = matching.Repeat(matching.Or([W, T]))
        text = 'abcWWTWT2'
        self.match(text, matching.AnyBefore(rule), 8, 'WWTWT')

    def testGreedyReapeat(self):
        """
        Repeat eats up tokens of the next rule
        """
        W = matching.Token(self.get_token('W'))
        T = matching.Token(self.get_token('T'))
        rule = matching.Repeat(W)
        rule = matching.And([rule, W])

        text = 'WWTT'
        #self.match(text, rule, 2, 'W')  <-- this is correct
        self.match(text, rule, None, '')

    def testComplex(self):
        text = 'aaaAbbbBCDEbbbF'
        '*A(*A)*(BC)DE*F'
        A = matching.Token(self.get_token('A'))
        B = matching.Token(self.get_token('B'))
        C = matching.Token(self.get_token('C'))
        D = matching.Token(self.get_token('D'))
        E = matching.Token(self.get_token('E'))
        F = matching.Token(self.get_token('F'))
        starF = AnyBefore(F)
        starA = AnyBefore(A)
        starRest = AnyBefore(And([Optional(And([B, C])), D, E, starF]))
        rule = And([A, Optional(starA), starRest])
        rule = AnyBefore(rule)
        self.match(text, rule, len(text), 'ABCDEF')

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestMatching)
    unittest.TextTestRunner(verbosity=2).run(suite)