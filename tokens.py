﻿import pos

# Token - minimal sentence unit
class Token:
    def __init__(self, word, pos, function=None,
                 feature2=None, feature3=None, feature4=None,
                 feature5=None,
                 dictionary_form=None,
                 position=None):
        self.word = word
        self.pos = pos
        self.function = function
        self.feature2 = feature2
        self.feature3 = feature3
        self.feature4 = feature4
        self.feature5 = feature5
        self.position = position # set only if taken from an input stream
        self.dictionary_form = word if dictionary_form is None else dictionary_form

    def __str__(self):
        return self.word + '[' + pos.get_pos_name(self.pos) + ']'

    def __repr__(self):
        return self.__str__()

    def dump(self):
        return self.word + '[' + pos.get_pos_name(self.pos) + ']' + \
               ','.join(str(item) for item in [self.function, self.feature2])

class Particle(Token):
    def __init__(self, word, **kwargs):
        super().__init__(word, pos.PARTICLE, **kwargs)

class CaseParticle(Particle):
    def __init__(self, word):
        super().__init__(word, function=pos.FunctionFeature.CASE_PARTICLE)

class Auxiliary(Token):
    def __init__(self, word):
        super().__init__(word, pos.AUXILIARY)

class Noun(Token):
    def __init__(self, word, **kwargs):
        super().__init__(word, pos.NOUN, **kwargs)

class Pronoun(Noun):
    def __init__(self, word):
        super().__init__(word, function=pos.FunctionFeature.PRONOUN)

class CommonNoun(Token):
    def __init__(self, word):
        super().__init__(word, pos.NOUN, pos.FunctionFeature.ORDINARY)

class DependentNoun(Token):
    def __init__(self, word):
        super().__init__(word, pos.NOUN, pos.FunctionFeature.DEPENDENT)

class Verb(Token):
    def __init__(self, word):
        super().__init__(word, pos.VERB)

class Adverb(Token):
    def __init__(self, word):
        super().__init__(word, pos.ADVERB)

class AdjectiveI(Token):
    def __init__(self, word):
        super().__init__(word, pos.ADJI)

class AdjectiveNa(Token):
    def __init__(self, word):
        super().__init__(word, pos.ADJNA)

def main():
    pass

if __name__ == '__main__':
    main()
