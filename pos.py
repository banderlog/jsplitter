﻿# -*- coding: utf-8 -*-

UNKNOWN = 0
NOUN = 1
VERB = 2
ADJI = 3
ADJNA = 4
VERB = 5
ADVERB = 6
PRENOUN = 7
CONJUNCTION = 8
PARTICLE = 9
INTERJECTION = 10
AUXILIARY = 11
PREFIX = 12
SYMBOL = 100
FILLER = 101
OTHER = 200

class FunctionFeature:
    FUNC_ADVERB = '副詞化'

    CASE_PARTICLE        = '格助詞'
    CONNECTING_PARTICLE  = '係助詞'
    FINAL_PARTICLE       = '終助詞'
    CONJUNCTIVE_PARTICLE = '接続助詞'
    PARALLEL_PARTICLE = '並立助詞'
    MIXED_PARTICLE = '副助詞／並立助詞／終助詞'

    ORDINARY = '一般'
    PROPER_NOUN = '固有名詞'
    INDEPENDENT = '自立'
    DEPENDENT_VERB = '動詞非自立的'
    DEPENDENT = '非自立'
    NUMBER = '数'

    SPACE = '空白'
    BRACKET = '括弧閉'  # 記号
    ALPHABET = 'アルファベット'

    INTERJECTION = '間投'
    VSURU = 'サ変接続'      # 食事

    PRONOUN = '代名詞'

    QUOTE = '引用文字列'
    FULL_STOP = '句点'  # 記号
    COMMA = '読点'

    UNKNOWN = '*'
    SUFFIX = '接尾'

    ADVERB = '副助詞'
    VERB_AUX = '動詞接続'
    SPECIAL = '特殊'
    NAI_SUFFIX =  'ナイ形容詞語幹'
    NOUN_SUFFIX = '名詞接続'
    NUMERIC_SUFFIX = '数接続'
    ADJNA_ROOT = '形容動詞語幹'
    ADJNA_CONJ = '形容詞接続'
    UNION_MOD = '連体化'         # 食事 の
    VERB_CONJ = '助詞類接続'
    ADVERB_POSSIBILITY = '副詞可能'

# 3rd feature
class Feature3rd:
    HUMAN_NAME = '人名'
    GEOGRAPHIC_NAME = '地域'
    ORGANIZATION_NAME = '組織'
    QUOTE = '引用'
    COMPOUND = '連語'
    ORDINARY = '一般'
    SPECIAL = '特殊'
    CONTRACTION = '縮約'
    UNKNOWN = '*'
    COUNTER = '助数詞'

"""
# , , 'サ変接続', , , '副詞可能', '助動詞語幹', '形容動詞語幹', , , ,
"""

class Feature4:
    pass

"""
'*', '一般', '姓', '国', '名'
"""

jname_to_enum = {
    '名詞'   : (NOUN, 'n'),
    '形容詞' : (ADJI, 'adj-i'),
    '動詞'   : (VERB, 'v'),
    '副詞'   : (ADVERB, 'adv'),
    '連体詞' : (PRENOUN, 'pren-adj'),
    '接続詞' : (CONJUNCTION, 'conj'),
    '記号'   : (SYMBOL, 'sym'),
    '助動詞' : (AUXILIARY, 'aux'),
    '助詞'   : (PARTICLE, 'prt'),
    '感動詞' : (INTERJECTION, 'int'),
    '接頭詞' : (PREFIX, 'pref'),
    'その他' : (OTHER, 'other'),
    'フィラー': (FILLER, 'filler')
    }

pos_enum_to_str = dict(jname_to_enum.values())

def get_pos_pair(jname):
    return jname_to_enum.get(jname, (UNKNOWN, 'unk'))

def get_pos(jname):
    return get_pos_pair(jname)[0]

def get_pos_name(pos_enum):
    return pos_enum_to_str.get(pos_enum, 'unk')

def normalize_feature(feature):
    return None if feature == FunctionFeature.UNKNOWN else feature

#Feature 5

"""

 '五段・ナ行'
 '五段・バ行'
 '五段・ワ行促音便'
 '五段・ワ行ウ音便'
 '五段・カ行促音便'
 '五段・サ行'
 '五段・ガ行'
 '五段・ラ行'
 '五段・カ行促音便ユク'
 '五段・マ行'
 '五段・ラ行特殊'
 '五段・タ行'
 '五段・カ行イ音便'
 '五段・ラ行アル'

 '四段・ハ行'
 '一段'
 '一段・得ル'
 '一段・クレル'

 '特殊・タイ'
 '特殊・デス'
 '特殊・タ'
 '特殊・ヌ'
 '特殊・ジャ'
 '特殊・ヤ'
 '特殊・マス'
 '特殊・ダ'

 'サ変・－ズル'
 'カ変・来ル'
 'サ変・－スル'
 'カ変・クル'
 'サ変・スル'
 '特殊・ナイ'

 '形容詞・イ段'
 '形容詞・アウオ段'
 '形容詞・イイ'

 '文語・ケリ'
 '文語・ゴトシ'
 '文語・リ'
 '文語・ベシ'
 '文語・キ'
 '文語・ナリ'
 '文語・ル'

 '下二・タ行'
 '下二・ダ行'
 '上二・ダ行'
 '不変化型'
"""

# Feature 6

"""
'連用テ接続',
'命令ｉ',
'仮定縮約１',
'命令ｙｏ',
'現代基本形',
'基本形',
'命令ｅ',
'文語基本形',
'体言接続',
'体言接続特殊２',
'未然形',
'連用ニ接続',
'連用ゴザイ接続',
'未然ウ接続',
'基本形-促音便',
'*',
'未然ヌ接続',
'ガル接続',
'仮定形',
'連用タ接続',
'未然特殊',
'仮定縮約２',
'連用形',
'未然レル接続',
'命令ｒｏ',
'連用デ接続',
 '音便基本形',
 '体言接続特殊'
"""

def main():
    pass

if __name__ == '__main__':
    main()
