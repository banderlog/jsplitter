﻿
import tokenfactory
import textparser
import ruleparser

class Collector:
    def __init__(self):
        self.words = []

    def capture(self, token):
        self.words.append(token)

    def result(self):
        return self.words

def do():
    rule_text = 'と＋は'
    rule = ruleparser.parse(rule_text, tokenfactory.get_pattern, True)
    file_name = r'c:\project\books\_abe\_woman\dune_jp.txt'
    content = open(file_name, encoding='shift-jis').read()
    tp = textparser.TextParser(content)
    size = 0
    for sentence in tp.getSentences():
       # sentence = '「明日、私は朝寝坊するよ」' #sentence.strip()
        collector = Collector()
        words = list(tokenfactory.get_tokens_as_words(sentence))
        rule.match(words, 0, collector.capture, None)
        if len(collector.result()):
            print(collector.result(), sentence)
        size += 1
    print(size)


def main():
    do()

if __name__ == '__main__':
    main()
