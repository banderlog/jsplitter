﻿
import operator
from collections import namedtuple
from itertools import chain
from splitter import Sentence

Node = namedtuple('Node', ['chunk', 'childrens'])

#class Layer

def to_html_name(name):
    #return name
    n = name.encode('ascii', 'xmlcharrefreplace')
    return n.decode('ascii')

def create_nodes_with_text(layers, get_chunk_text_func):
    all_edges = []
    nodes_per_layer = {}
    for level, layer in enumerate(layers):
        for parent_id, chunks in sorted(layer.items()):
            text = get_chunk_text_func(parent_id, True)
            d = nodes_per_layer.setdefault(level, {})
            d[parent_id] = text
            s = nodes_per_layer.setdefault(level + 1, {})
            for child_id in chunks:
                s[child_id] = get_chunk_text_func(child_id, True)
            extra_node_id = parent_id + 1000
            s[extra_node_id] = get_chunk_text_func(parent_id, False)
            all_edges.extend((parent_id, chunk_id) for chunk_id in sorted(chunks))
            all_edges.append((parent_id, extra_node_id))
    return nodes_per_layer, all_edges

def to_dot(layers, get_chunk_text_func):
    nodes_per_layer, edges = create_nodes_with_text(layers, get_chunk_text_func)
    out = 'digraph Sentence '+ '\n' + \
          '{' + '\n' + \
          ' ' + '\n'
    for level, chunk_ids in sorted(nodes_per_layer.items()):
        out += ' { rank = same;' + '\n'
        for chunk_id, text in sorted(chunk_ids.items()):
            decorated_text = to_html_name(text)
            shape = 'shape=box, style="rounded",fontsize=11'
            out += ' ' * level + '{{ node [{0}, label="{1}"] N{2}; }} \n'.format(shape, decorated_text, chunk_id)
        out += ' }' + '\n'
    for from_chunk, to_chunk in edges:
        out += ' N{0} -> N{1};\n'.format(from_chunk, to_chunk)
    out += '}'
    return out


class LayeredTree:
    def __init__(self, sentence):
        self.sentence = sentence
        self.links_per_chunk = self.create_links_per_chunk()
        self.text_per_chunk = {}
        self.create_text_per_chunk(sentence.chunks, self.links_per_chunk, sentence.root_id, self.text_per_chunk)

    ###############################
    ## Internal functions
    ################################
    def create_text_per_chunk(self, chunks, links_per_chunk, root_id, text_per_chunk):
        layer_chunk_ids = links_per_chunk.get(root_id, [])
        if len(layer_chunk_ids) == 0:
            text_per_chunk[root_id] = chunks[root_id].get_text()
            return
        for chunk_id in layer_chunk_ids:
            self.create_text_per_chunk(chunks, links_per_chunk, chunk_id, text_per_chunk)

        texts = [text_per_chunk[chunk_id] for chunk_id in layer_chunk_ids]
        title = ''.join(texts) + chunks[root_id].get_text()
        text_per_chunk[root_id] = title

    def create_links_per_chunk(self):
        links_per_chunk = {}
        for link in self.sentence.links:
            lower, upper = link
            chunk_links = links_per_chunk.setdefault(upper, [])
            chunk_links.append(lower)
            links_per_chunk[upper] = chunk_links
        return links_per_chunk

    def create_layers_internal(self, links, layers, level, chunk_id):
        reduced_links = []
        my_children = set()
        for link in links:
            if link[1] == chunk_id:
                my_children.add(link[0])
            else:
                reduced_links.append(link)
        if len(my_children) == 0:
            return
        layer = layers.setdefault(level, {})
        layer[chunk_id] = my_children
        for chunk_id in my_children:
            self.create_layers_internal(reduced_links, layers, level + 1, chunk_id)

    def get_chunk_text_func(self, chunk_id, full_text):
        return self.text_per_chunk[chunk_id] if full_text else self.sentence.chunks[chunk_id].get_text()

    ## Interface functions

    def get_layer_text(self, delimiter):
        nodes = self.get_nodes_with_text()
        for level, layer in sorted(nodes.items()):
            yield delimiter.join( text for (_, text) in sorted(layer.items()) )

    def get_layers(self):
        layers = {}
        self.create_layers_internal(self.sentence.links, layers, 0, self.sentence.root_id)
        return list(layer for _, layer in sorted(layers.items()))

    def get_nodes_with_text(self):
        layers = self.get_layers()
        nodes, _ = create_nodes_with_text(layers, self.get_chunk_text_func)
        return nodes

    def to_dot(self):
        layers = self.get_layers()
        return to_dot(layers, self.get_chunk_text_func)

def main():
    pass

if __name__ == '__main__':
    main()
